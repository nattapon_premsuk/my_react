import React, { Component } from 'react';
import './App.css';

import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import Example from './app/components/Example/Example';
import DataFilter from './app/components/Example/DataFilter'
import Axios from 'axios';
import { BrowserRouter, Route, Link, Router } from "react-router-dom";

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  card: {
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 10,
  }
}));

class App extends Component {


  constructor(props) {
    super(props)

    this.state = {
      row: [],
      carBrands: [
        {
          carId: '01',
          carBrand: 'NISSAN',
        },
        {
          carId: '02',
          carBrand: 'TOYOTA',
        },
        {
          carId: '03',
          carBrand: 'MITSUMISHI',
        }],
      carType: [{
        carBrand: 'TOYOTA',
        carType: '02',
        carTypeName: 'YARIS 1.2'

      }, {
        carBrand: 'NISSAN',
        carType: '01',
        carTypeName: 'AMELA 1.0 Tur'

      },{
        carBrand: 'MITSUMISHI',
        carType: '03',
        carTypeName: 'NITRON 2.0 Tur'

      }
      ],

      data: [
        {
          carBrand: 'TOYOTA',
          carType: 'YARIS 1.2',
          title: 'คนขับอายุ 30 ขึ้นไป / ติดกล้อง / ไม่ใช้รถในเชิงธุรกิจ',
          price: '8,120',
          insurent: '490,000',
          fixtype: 'อู่'
        },
        {
          carBrand: 'TOYOTA',
          title: 'คนขับอายุ 30 ขึ้นไป / ติดกล้อง / ไม่ใช้รถในเชิงธุรกิจ',
          carType: 'YARIS 1.5',
          price: '13,130',
          insurent: '490,000',
          fixtype: 'อู่'
        }, {
          carBrand: 'NISSAN',
          title: 'คนขับอายุ 30 ขึ้นไป / ติดกล้อง / ไม่ใช้รถในเชิงธุรกิจ',
          carType: 'AMELA 1.0 Tur',
          price: '5,920',
          insurent: '170,000',
          fixtype: 'อู่'
        }, {
          carBrand: 'NISSAN',
          title: 'คนขับอายุ 30 ขึ้นไป / ติดกล้อง / ไม่ใช้รถในเชิงธุรกิจ',
          carType: 'AMELA 1.0 Tur',
          price: '2,613',
          insurent: '-',
          fixtype: '-'
        }, {
          carBrand: 'MITSUMISHI',
          title: 'อ็ม เอส ไอ จี ประกันชั้น 3+',
          carType: '',
          price: '1,658',
          insurent: '100,000',
          fixtype: 'อู่'
        }
      ],
      dataFilter: [],
      dataCarTypeFilter: [],
      selectedCardBrand: 'NISSAN',
      selectedCardType: '',
      height: props.height,
      width: props.width
    }
  }

  componentDidMount = () => {
    this.onFilBrand(this.state.selectedCardBrand)
  }


  search = (keyword) => {
    console.log(keyword);
    var dataArray = [];
    var urlApi = 'https://api.themoviedb.org/3/search/movie?api_key=accb4474b0b6037b1a5c9f2713cb8815&query=' + keyword;
    Axios.get(urlApi).then(res => {
      res.data.results.forEach(element => {
        element.poster_src = "https://image.tmdb.org/t/p/w500" + element.poster_path;
        console.log(element.poster_src);
        dataArray.push(element);
      });

      this.setState({ row: dataArray })
    })
  }

  onFilBrand = (value) => {
    let data = this.state.carType.filter(x => x.carBrand === value)
    this.setState({
      selectedCardBrand: value,
      dataCarTypeFilter: data
    })
  }

  onFilter = (carBrand) => {

    let inserttData = this.state.data.filter(x => x.carBrand === carBrand)
    this.setState({
      dataFilter: inserttData
    })
  }

  render() {
    return (
      <div className="App" >
        <br />
        <h1>ประกันรถยนต์ {this.state.selectedCardBrand}</h1>
        {/* <TextField onChange={(event) => { this.search(event.target.value) }} id="outlined-basic" label="Search" variant="outlined" /> */}
        <br />

        <select style={{ width: 150, height: 30, margin: 10 }} onChange={(event) => {
          this.onFilBrand(event.target.value)
        }}>
          {
            this.state.carBrands.map(item => {

              return (
                <option value={item.carBrand}>{item.carBrand}</option>
              )
            })
          }

        </select>

        <select disabled={this.state.selectedCardBrand === ''} style={{ width: 150, height: 30, margin: 10 }} onChange={(event) => {
          this.setState({
            selectedCardType: event.target.value
          })
        }}>
          {
            this.state.dataCarTypeFilter.map(item => {

              return (
                <option value={item.carType}>{item.carTypeName}</option>
              )
            })
          }

        </select>



        <button style={{ width: 100, height: 30, backgroundColor: '#ffa400', color: 'white' }} onClick={() => this.onFilter(this.state.selectedCardBrand)}>
          ค้นหา
        </button>


        <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", gridGap: 20}}>
          {

            this.state.dataFilter.map(item => { 

              return (
                <div key={''} >
                  <h1>{item.carBrand} {item.carType}</h1>
                  <h3>{item.title}</h3>
                  <p>ราคาล่าสุด ฿{item.price} <span>ต่อปี</span></p>
                  <p>ทุนประกัน {item.insurent}</p>
                  <p>ประเภทการซ่อม {item.fixtype}</p>
                </div>
              )

            })
          }
        </div>
        {/* {this.state.row.map(item => (
          <div>
            <Example data={item}></Example>
          </div>

        ))} */}
      </div>

    );
  }
}

export default App;
