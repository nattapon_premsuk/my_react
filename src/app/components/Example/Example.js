import React, { Component } from 'react'
import Avatar from '@material-ui/core/Avatar';

export default class Example extends Component {

    constructor(props) {

        super(props)

    }
    render() {
        return ( 
            <div key={this.props.data.id}>
                <h1>{this.props.data.title}</h1>
                <img src={this.props.data.poster_src} />
                <p>{this.props.data.overview}</p>
            </div>
        )
    }
}
